import React from "react";
import PropTypes from 'prop-types'
const MyButton = ({value,action,style}) => {
    return(
        <button onClick={action} style={style}>{value}</button>
    );
}
MyButton.defaultProps = {
    value:"none",
    action:(()=>{console.error('Non action')}),
    style:{}
}
MyButton.propTypes = {
    action:PropTypes.func,
    value:PropTypes.string,
    style:PropTypes.object
}
export default MyButton;