import React from "react";
import PropTypes from 'prop-types'
import MyButton from "./task1";
class JsonReceiver extends React.Component {
    state={usersList:[]}
    constructor(props) {
        super(props);
    }

    //TODO Example data for json
// {
//     users:
//         [
//             {id:1,name:"John",surname:"Willis"},
//             {id:2,name:"Anna",surname:"Sovakot"},
//             {id:3,name:"Andrew",surname:"Ponchik"},
//             {id:4,name:"Lisa",surname:"Rashunina"},
//             {id:5,name:"Max",surname:"Lohovoy"},
//             {id:6,name:"Alex",surname:"Smerd"},
//             {id:7,name:"Victorya",surname:"Secret"}
//
//         ]
// }
    interviewHandler = (id) => _ => {
        const{usersList} = this.state;
        const newUsersList = usersList.map(item => {
            const{user,interviewed} = item;
            if(user.id === id){
                return {...item,interviewed:!interviewed};
            }else
                return {...item};
        });
        this.setState({usersList:newUsersList});
    }
    componentDidMount() {
        fetch("http://www.json-generator.com/api/json/get/cfWvsmSuiG?indent=2").then(response=> response.json()).
        then(data=>{this.setState({usersList:data.users.map(item=>({user:item,interviewed:false}))})});
    }

    render() {
        const{usersList} = this.state;
        return (
            <div>
                <ul>
                    {usersList.map(item => (
                        <User key={item.user.id} user={item.user} interviewed={item.interviewed} interviewHandler={this.interviewHandler}/>
                    ))}
                </ul>

            </div>
        );
    }

}

const User = ({user,interviewed,interviewHandler}) => {
    return(
        <li>
            <div>
                <h1 className={interviewed?"interviwed":"not-interviwed"}>{`${user.name} ${user.surname}`}</h1>
                <MyButton value="check" action={interviewHandler(user.id)}/>
            </div>
        </li>


    );
}
export default JsonReceiver;