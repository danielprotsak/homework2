import React,{Component} from "react";
import PropTypes from 'prop-types'

export default class Table extends Component{
    constructor(props) {
        super(props);
    }

    render = ()=> {
        const{children,border} = this.props;
        return(
            <table style={{border:border}}>
                <tbody>
                {children}
                </tbody>
            </table>
        );
    }

}

export const Row = ({head,children,border})=>{

    return(
        <tr style={{border:border}}>
            {head &&
                <th>
                    {children}
                </th>
            }
            {!head &&
                children
            }
        </tr>
    );
}
export const Cell = ({colspan,type,background,color,children,currency,border})=>{

    return(
        <td colSpan={colspan} style={{background: background, color: color, width:40}}>
            {type==="DATE" &&
                <i>{children}</i>
            }
            {type === "NUMBER" &&
                <span style={{float:"right"}}>{children}</span>
            }
            {type === "MONEY" &&
                    <span style={{float:"right"}}>{children}{currency===undefined?console.error("Currency props is not defined in component"):currency}</span>
            }
            {type === "TEXT" &&
            <span style={{float:"left"}}>{children}</span>
            }
        </td>
    );
}
Row.defaultProps = {
    head:false,
}
Row.propTypes = {
    children:PropTypes.array,
}
Table.propTypes = {
    children:PropTypes.oneOfType([
        PropTypes.instanceOf(Row),
        PropTypes.array
    ]),
    border:PropTypes.string
}
Cell.propTypes = {
    colspan:PropTypes.number,
    type:PropTypes.oneOf(['DATE','NUMBER','MONEY','TEXT']).isRequired,
    background:PropTypes.string,
    color:PropTypes.string,
    currency:PropTypes.string,
}
Cell.defaultProps={
    colspan:0,
    type:"TEXT",
    background:"none",
    color:"black",
}