import React,{Component} from "react";
import PropTypes from 'prop-types'

export default class ImgWithPreLoader extends Component{
    state = {img:null,isReady:false,text:"Loading"}
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        const{url} = this.props;
            const interval = setInterval(()=>{
                const{text} = this.state;
                const newText = text.includes("...")?text.replace("...",""):text.concat("",".");
                this.setState({text:newText});
            },500)
            setTimeout(()=>{
                clearInterval(interval);
                let img = new Image();
                img.src=url;
                img.onload = ()=>{
                    this.setState({img:img,isReady:true})
                }
            },5000)

    }
    render = () => {
        const {isReady,img,text} = this.state;
        return(
            <div>
                {!isReady &&
                <h1>{text}</h1>
                }
                {isReady &&
                <img src={img.src} alt=""/>
                }
            </div>
        );
    }

}
ImgWithPreLoader.defaultProps = {
    url:"https://itea.ua/wp-content/themes/new-it/images/about_us/panda_for_main.gif"
}
ImgWithPreLoader.propTypes = {
    url: PropTypes.string
}

